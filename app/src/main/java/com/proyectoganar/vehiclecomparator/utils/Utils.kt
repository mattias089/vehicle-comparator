package com.proyectoganar.vehiclecomparator.utils


import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.viewinterop.AndroidView
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.core.radar.series.Line
import com.anychart.data.Set
import com.anychart.enums.Align
import com.anychart.enums.MarkerType
import com.neo.arcchartview.ArcChartView
import com.proyectoganar.vehiclecomparator.model.CustomDataEntry

/**
 * Created by Matias Arancibia on 09 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */

@Composable
fun ColouredRadarChart() {
    AndroidView(
        factory = { context ->
            val radarChartView = ArcChartView(context)

            radarChartView.sectionsCount = 5

            radarChartView
        }
    )
}

@Composable
fun RadarChart() {
    AndroidView(
        factory = { context ->
            val anyChartView = AnyChartView(context)
            val radar = AnyChart.radar()

            radar.title("WoW base stats comparison radar chart: Shaman vs Warrior vs Priest")

            radar.yScale().minimum(0.0)
            radar.yScale().minimumGap(0.0)
            radar.yScale().ticks().interval(50.0)

            radar.xAxis().labels().padding(5.0, 5.0, 5.0, 5.0)

            radar.legend()
                .align(Align.CENTER)
                .enabled(true)

            val data: MutableList<DataEntry> = ArrayList()
            data.add(CustomDataEntry("Strength", 136, 199))
            data.add(CustomDataEntry("Agility", 79, 125))
            data.add(CustomDataEntry("Stamina", 149, 173))
            data.add(CustomDataEntry("Intellect", 135, 33))
            data.add(CustomDataEntry("Spirit", 158, 64))

            val set = Set.instantiate()
            set.data(data)

            val shamanData = set.mapAs("{ x: 'x', value: 'value' }")
            val warriorData = set.mapAs("{ x: 'x', value: 'value2' }")
//            val priestData = set.mapAs("{ x: 'x', value: 'value3' }")

            val shamanLine: Line = radar.line(shamanData)
            shamanLine.name("Shaman")
            shamanLine.markers()
                .enabled(true)
                .type(MarkerType.CIRCLE)
                .size(3.0)

            val warriorLine: Line = radar.line(warriorData)
            warriorLine.name("Warrior")
            warriorLine.markers()
                .enabled(true)
                .type(MarkerType.CIRCLE)
                .size(3.0)

//            val priestLine: Line = radar.line(priestData)
//            priestLine.name("Priest")
//            priestLine.markers()
//                .enabled(true)
//                .type(MarkerType.CIRCLE)
//                .size(3.0)

            radar.tooltip().format("Value: {%Value}")

            anyChartView.setChart(radar)
            anyChartView
        }
    )
}

@Composable
fun AutoSizeText(
    text: String,
    textStyle: TextStyle,
    modifier: Modifier = Modifier
) {
    var scaledTextStyle by remember { mutableStateOf(textStyle) }
    var readyToDraw by remember { mutableStateOf(false) }

    Text(
        text,
        modifier.drawWithContent {
            if (readyToDraw) {
                drawContent()
            }
        },
        style = scaledTextStyle,
        softWrap = false,
        onTextLayout = { textLayoutResult ->
            if (textLayoutResult.didOverflowWidth) {
                scaledTextStyle =
                    scaledTextStyle.copy(fontSize = scaledTextStyle.fontSize * 0.9)
            } else {
                readyToDraw = true
            }
        }
    )
}