package com.proyectoganar.vehiclecomparator

import android.app.Activity
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ManageSearch
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.CardElevation
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import coil.compose.AsyncImage
import com.proyectoganar.vehiclecomparator.model.ColumnViewData
import com.proyectoganar.vehiclecomparator.ui.COMFORT_PARAM
import com.proyectoganar.vehiclecomparator.ui.PERFORMANCE_PARAM
import com.proyectoganar.vehiclecomparator.ui.SAFETY_PARAM
import com.proyectoganar.vehiclecomparator.ui.SPEED_PARAM
import com.proyectoganar.vehiclecomparator.ui.CommentsView
import com.proyectoganar.vehiclecomparator.ui.SpiderWebChart
import com.proyectoganar.vehiclecomparator.ui.theme.LightestGray
import com.proyectoganar.vehiclecomparator.ui.theme.PrimaryColor
import com.proyectoganar.vehiclecomparator.ui.theme.VehicleComparatorTheme
import com.proyectoganar.vehiclecomparator.ui.theme.getColorScheme
import com.proyectoganar.vehiclecomparator.utils.AutoSizeText
import java.text.DecimalFormat
import java.text.NumberFormat

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            VehicleComparatorTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Content()
                }
            }
        }
    }
}

private val data = mutableListOf(
    ColumnViewData(
        imageUrl = "https://carsales.pxcrush.net/carsales//car/dealer/0471096806f04f8837036ce5a1ef6ee2.jpg?pxc_method=gravityfill&pxc_bgtype=self&pxc_size=720,480",
        make = "Audi",
        model = "A5",
        price = 40990.0,
        values = listOf(
            "2.0L",
            "Automatic",
            "Turbo Petrol",
            "65.304",
            "2016",
            "Coupe",
            "Black",
            "2",
            "4",
        )
    ),
    ColumnViewData(
        make = " ",
        model = " ",
        price = 0.0,
        values = listOf(
            "Engine",
            "Transmission",
            "Fuel",
            "Kilometres",
            "Release date",
            "Body Style",
            "Colour",
            "Doors",
            "Seats",
        )
    ),
    ColumnViewData(
        imageUrl = "https://carsales.pxcrush.net/carsales/cars/private/37ap2thlqy86pe4eimemoywpd.jpg?pxc_method=fit&pxc_size=1795%2c1195",
        make = "Mercedes-Benz",
        model = "500SL",
        price = 59000.0,
        values = listOf(
            "5.0L",
            "Automatic",
            "Petrol",
            "116.500",
            "1990",
            "Convertible",
            "Red",
            "2",
            "2",
        )
    )
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Content() {
    val activity = (LocalContext.current as? Activity)
    var showDialog by remember { mutableStateOf(false) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = if (showDialog) {
                            "Comparison features"
                        } else {
                            stringResource(id = R.string.top_app_bar_title)
                        },
                        color = Color.Black,
                        fontSize = 17.sp,
                        fontWeight = FontWeight.Normal
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            if (showDialog) {
                                showDialog = false
                            } else {
                                activity?.finish()
                            }
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "backIcon",
                            tint = PrimaryColor
                        )
                    }
                },
                actions = {
                    if (!showDialog) {
                        IconButton(
                            onClick = {
                                showDialog = true
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Filled.ManageSearch,
                                contentDescription = null,
                                tint = PrimaryColor
                            )
                        }
                    } else {
                        IconButton(
                            onClick = {
                                showDialog = false
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.Save,
                                contentDescription = null,
                                tint = PrimaryColor
                            )
                        }
                    }
                },
                backgroundColor = Color.White,
                contentColor = Color.White,
                elevation = 4.dp
            )
        },
        content = { innerPadding ->
            var dialogTitle: String? by rememberSaveable { mutableStateOf(null) }

            Column(
                modifier = Modifier.padding(innerPadding),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(15.dp)
                ) {
                    data.forEachIndexed { index, columnVD ->
                        DataColumn(
                            modifier = Modifier.weight(1f),
                            columnVD = columnVD,
                            index = index
                        )
                    }
                }

                Row(
                    horizontalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    Box(
                        modifier = Modifier
                            .size(size = 20.dp)
                            .background(Color(0xFFFFA478))
                    )
                    Text(
                        text = "${data[0].make} ${data[0].model}",
                        fontWeight = FontWeight.Bold
                    )

                    Spacer(modifier = Modifier.width(5.dp))

                    Box(
                        modifier = Modifier
                            .size(size = 20.dp)
                            .background(Color(0xFF827EBE))
                    )
                    Text(
                        text = "${data[2].make} ${data[2].model}",
                        fontWeight = FontWeight.Bold
                    )
                }

                Spacer(modifier = Modifier.height(10.dp))

                Box {
                    SpiderWebChart() { textClicked ->
                        dialogTitle = textClicked
                    }
                }
            }

            //Comments Dialog
            dialogTitle?.let {
                Dialog(
                    properties = DialogProperties(
                        dismissOnBackPress = true,
                        dismissOnClickOutside = true,
                        usePlatformDefaultWidth = false
                    ),
                    onDismissRequest = { dialogTitle = null },
                ) {
                    Box(
                        modifier = Modifier.fillMaxSize().background(Color.White),
                    ) {
                        CommentsView(it)
                    }
                }
            }

            if (showDialog) {
                FeaturesDialog()
            }
        }
    )
}

@Composable
fun FeaturesDialog() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
            .padding(20.dp)
    ) {
        Column {
            Text(
                text = "Select your preferences to be used in the graph:",
                fontSize = 16.sp
            )

            Spacer(Modifier.height(20.dp))

            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                LabeledCheckbox(label = SPEED_PARAM, modifier = Modifier.weight(1f), checked = true)
                LabeledCheckbox(label = "Acceleration", modifier = Modifier.weight(1f), checked = false)
            }
            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                LabeledCheckbox(label = SAFETY_PARAM, modifier = Modifier.weight(1f), checked = true)
                LabeledCheckbox(label = PERFORMANCE_PARAM, modifier = Modifier.weight(1f), checked = true)
            }
            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                LabeledCheckbox(label = COMFORT_PARAM, modifier = Modifier.weight(1f), checked = true)
                LabeledCheckbox(label = "Sound insulation", modifier = Modifier.weight(1f), checked = true)
            }
            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                LabeledCheckbox(label = "Inner space", modifier = Modifier.weight(1f), checked = false)
                LabeledCheckbox(label = "Engine power", modifier = Modifier.weight(1f), checked = false)
            }
        }
    }
}

@Composable
fun LabeledCheckbox(
    modifier: Modifier = Modifier,
    checked: Boolean,
    label: String
) {
    var isChecked by remember { mutableStateOf(checked) }

    Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            checked = isChecked,
            colors = CheckboxDefaults.colors(
                checkedColor = PrimaryColor
            ),
            onCheckedChange = {
                isChecked = !isChecked
            }
        )

        Text(
            modifier = Modifier.wrapContentWidth(),
            text = label
        )
    }
}

@Composable
fun DataColumn(
    modifier: Modifier,
    columnVD: ColumnViewData,
    index: Int
) {
    Card(
        modifier = modifier.wrapContentHeight(),
        shape = CutCornerShape(8.dp),
        elevation = if (columnVD.imageUrl != null) {
            CardDefaults.elevatedCardElevation()
        } else {
            CardDefaults.elevatedCardElevation(
                defaultElevation = 0.dp
            )
        },
        colors = if (columnVD.imageUrl != null) {
            CardDefaults.cardColors(
                containerColor = Color.White
            )
        } else {
            CardDefaults.cardColors(
                containerColor = Color.Transparent
            )
        }
    ) {
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .padding(15.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            // Vehicle image
            columnVD.imageUrl?.let {
                Card(
                    shape = RoundedCornerShape(5.dp),
                    border = BorderStroke(1.dp, Color.Gray)
                ) {
                    AsyncImage(
                        modifier = Modifier
                            .size(100.dp, 60.dp)
                            .background(Color(0xFFEDEDED)),
                        model = it,
                        contentDescription = null
                    )
                }
            } ?: run {
                Box(modifier = Modifier.size(100.dp, 50.dp))
            }

            Spacer(modifier = Modifier.height(10.dp))

            // Make and Model
            if (columnVD.make != null && columnVD.model != null) {
                AutoSizeText(
                    text = columnVD.make,
                    textStyle = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center
                    )
                )

                AutoSizeText(
                    text = columnVD.model,
                    textStyle = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center
                    )
                )

                Spacer(modifier = Modifier.height(5.dp))
            } else {
                Spacer(modifier = Modifier.height(27.dp))
            }

            // Vehicle Price
            if (columnVD.price != null && columnVD.price > 0.0) {
                AutoSizeText(
                    text = "$${formatPrice(columnVD.price)}",
                    textStyle = TextStyle(
                        fontSize = 17.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        fontStyle = FontStyle.Italic,
                        color = getColorScheme().primary
                    )
                )

                Spacer(modifier = Modifier.height(20.dp))
            } else {
                Spacer(modifier = Modifier.height(53.dp))
            }

            // Vehicle specifications
            Column(
                verticalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                columnVD.values?.forEachIndexed { valuePosition, value ->
                    AutoSizeText(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(
                                if (index != 1 && ((valuePosition % 2) != 0)) {
                                    LightestGray
                                } else {
                                    Color.Transparent
                                }
                            ),
                        text = value,
                        textStyle = TextStyle(
                            color = if (index != 1) {
                                Color.Black
                            } else {
                                getColorScheme().primary
                            },
                            fontSize = 15.sp,
                            fontWeight = when (index) {
                                0, 2 -> {
                                    FontWeight.Normal
                                }

                                else -> {
                                    FontWeight.Bold
                                }
                            },
                            textAlign = TextAlign.Center
                        )
                    )
                }
            }
        }
    }
}

@Composable
private fun ShowComments(title: String) {

}

@Composable
private fun formatPrice(price: Double): String {
    val formatter: NumberFormat = DecimalFormat("#,###")
    return formatter.format(price)
}

//@Preview(showBackground = true)
@Composable
fun FeaturesDialogPreview() {
    FeaturesDialog()
}

//@Preview(showBackground = true)
@Composable
fun DataColumnPreview() {
    DataColumn(Modifier, data[0], 1)
}

@Preview(showBackground = true)
@Composable
fun ContentPreview() {
    VehicleComparatorTheme {
        Content()
    }
}