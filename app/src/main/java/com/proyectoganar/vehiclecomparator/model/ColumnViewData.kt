package com.proyectoganar.vehiclecomparator.model

/**
 * Created by Matias Arancibia on 09 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */
data class ColumnViewData(
    val imageUrl: String? = null,
    val make: String? = null,
    val model: String? = null,
    val price: Double? = null,
    val values: List<String>? = null,
    val painterId: Int? = null
)