package com.proyectoganar.vehiclecomparator.model

/**
 * Created by Matias Arancibia on 10 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */
internal class Score {
    var score: Float
    var label: String? = null
    var iconId = 0

    constructor(score: Float, iconId: Int) {
        this.score = score
        this.iconId = iconId
    }

    constructor(label: String?, score: Float) {
        this.label = label
        this.score = score
    }

    constructor(iconId: Int, label: String?, score: Float) {
        this.iconId = iconId
        this.label = label
        this.score = score
    }
}