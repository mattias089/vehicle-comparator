package com.proyectoganar.vehiclecomparator.model

import com.anychart.chart.common.dataentry.DataEntry

/**
 * Created by Matias Arancibia on 10 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */
internal class CustomDataEntry(key: String, value1: Number, value2: Number) : DataEntry() {

    init {
        setValue(key, value1)
        setValue(key, value2)
    }
}