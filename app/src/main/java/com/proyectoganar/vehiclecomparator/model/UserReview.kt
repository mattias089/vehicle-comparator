package com.proyectoganar.vehiclecomparator.model

data class UserReview(
    val name: String,
    val score: Float,
    val comment: String
)