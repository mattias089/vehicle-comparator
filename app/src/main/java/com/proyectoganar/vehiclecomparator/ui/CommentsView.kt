package com.proyectoganar.vehiclecomparator.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.proyectoganar.vehiclecomparator.model.UserReview
import com.proyectoganar.vehiclecomparator.ui.theme.VehicleComparatorTheme

@Composable
fun CommentsView(
    title: String
) {
    val userReviews = userReviewFactory()

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = title,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                )
            )

            ScoreDrawer(score = userReviews.calculateScore())


        }

        AdPlaceholder()

        LazyColumn(
            modifier = Modifier.padding(6.dp),
            state = rememberLazyListState(),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            items(userReviews) {
                CommentView(userReview = it)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ContentPreview() {
    VehicleComparatorTheme {
        CommentsView("Velocidad")
    }
}

private fun List<UserReview>.calculateScore(): Float {
    var scoreSum = 0f
    this.forEach {
        scoreSum += it.score
    }

    return scoreSum / this.size
}

private fun userReviewFactory(): List<UserReview> = listOf(
    UserReview(
        name = "Aiden\nCampbell",
        score = 4f,
        comment = "Absolutely in love with my Audi A5! The sleek exterior, luxurious interior, and powerful engine make it a joy to drive. A true statement of style and performance."
    ),
    UserReview(
        name = "Alex\nJones",
        score = 4f,
        comment = "The Audi A5 exceeds all expectations. The handling is superb, the cabin is luxurious, and the technology features are top-notch. It's the perfect blend of elegance and excitement."
    ),
    UserReview(
        name = "Annabelle\nStewart",
        score = 5f,
        comment = "The Audi A5 is a true head-turner. The attention to detail in the design, the comfortable seating, and the intuitive infotainment system create a driving experience like no other."
    ),
    UserReview(
        name = "Nathan\nIrwin",
        score = 5f,
        comment = "Proud owner of an Audi A5. It's a beautiful car inside and out, and the driving experience is exceptional. The balance between comfort and performance is spot on."
    ),
    UserReview(
        name = "Elise\nWard",
        score = 2f,
        comment = "Driving the Audi A5 is pure pleasure. The powerful engine delivers exhilarating performance, while the luxurious interior and cutting-edge technology provide comfort and convenience."
    )
)