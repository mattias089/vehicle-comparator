package com.proyectoganar.vehiclecomparator.ui

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarBorder
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.proyectoganar.vehiclecomparator.ui.theme.PrimaryColor

@Composable
fun ScoreDrawer(score: Float) {
    Row {
        for (i in 1..5) {
            Icon(
                modifier = Modifier.size(25.dp),
                tint = Color(0xFFFDD835),
                imageVector = if (score >= i) {
                    Icons.Default.Star
                } else {
                    Icons.Default.StarBorder
                },
                contentDescription = null,
            )
        }

        Spacer(modifier = Modifier.width(12.dp))

        Text(
            text = "$score/5",
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight.Medium,
                color = PrimaryColor
            )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ScoreDrawerPreview() {
    ScoreDrawer(4.5f)
}
