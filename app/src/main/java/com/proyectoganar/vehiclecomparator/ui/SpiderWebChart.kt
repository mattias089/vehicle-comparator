package com.proyectoganar.vehiclecomparator.ui

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.proyectoganar.vehiclecomparator.R
import com.proyectoganar.vehiclecomparator.model.Score
import me.panpf.swsv.CircularLayout
import me.panpf.swsv.SpiderWebScoreView


/**
 * Created by Matias Arancibia on 10 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */

const val SPEED_PARAM = "Velocity"
const val SAFETY_PARAM = "Safety"
const val PERFORMANCE_PARAM = "Performance"
const val COMFORT_PARAM = "Comfort"
const val SOUND_INSULATION_PARAM = "Sound\nInsulation"

@Composable
fun SpiderWebChart(
    onClickCallback: ((clickedElement: String) -> Unit)
) {
    AndroidView(
        factory = { context ->
            val spiderWebScoreView1 = SpiderWebScoreView(context)
            val spiderWebScoreView2 = SpiderWebScoreView(context)
            val circularLayout = CircularLayout(context)
            val width = 600
            val height = 600

            spiderWebScoreView1.layoutParams = ViewGroup.LayoutParams(width, height)
            spiderWebScoreView2.layoutParams = ViewGroup.LayoutParams(width, height)
            circularLayout.layoutParams = ViewGroup.LayoutParams(width, height)

            val scores1 = arrayListOf(
                Score(R.drawable.ic_info, SPEED_PARAM, 4.7f),
                Score(R.drawable.ic_info, SAFETY_PARAM, 4.3f),
                Score(R.drawable.ic_info, PERFORMANCE_PARAM, 3.3f),
                Score(R.drawable.ic_info, COMFORT_PARAM, 2.8f),
                Score(R.drawable.ic_info, SOUND_INSULATION_PARAM, 3.0f),
            )
            val scores2 = arrayListOf(
                Score(R.drawable.ic_info, SPEED_PARAM, 3.5f),
                Score(R.drawable.ic_info, SAFETY_PARAM, 2.1f),
                Score(R.drawable.ic_info, PERFORMANCE_PARAM, 4.5f),
                Score(R.drawable.ic_info, COMFORT_PARAM, 4.8f),
                Score(R.drawable.ic_info, SOUND_INSULATION_PARAM, 3.4f),
            )

            setup(
                context,
                spiderWebScoreView1,
                spiderWebScoreView2,
                circularLayout,
                scores1,
                scores2,
                onClickCallback
            )

            val container = RelativeLayout(context).apply {
                layoutParams = RelativeLayout.LayoutParams(WRAP_CONTENT, 1000)
                clipChildren = false
                gravity = Gravity.CENTER
            }

            container.addView(spiderWebScoreView1)
            container.addView(spiderWebScoreView2)
            container.addView(circularLayout)

            container
        }
    )
}

private fun setup(
    context: Context,
    spiderWebScoreView1: SpiderWebScoreView,
    spiderWebScoreView2: SpiderWebScoreView,
    circularLayout: CircularLayout,
    scores1: ArrayList<Score>,
    scores2: ArrayList<Score>,
    onClickCallback: ((clickedElement: String) -> Unit)
) {
    val scoreArray1 = FloatArray(scores1.size)
    val scoreArray2 = FloatArray(scores2.size)

    for (i in scores1.indices) {
        scoreArray1[i] = scores1[i].score
    }

    for (j in scores2.indices) {
        scoreArray2[j] = scores2[j].score
    }

    spiderWebScoreView1.setScoreColor(R.color.primaryColor)
    spiderWebScoreView1.setLineColor(R.color.primaryColor)
    spiderWebScoreView1.setScoreStrokeColor(R.color.primaryColor)

    spiderWebScoreView1.setScores(5.0f, scoreArray1)
    spiderWebScoreView2.setScores(5.0f, scoreArray2)
    circularLayout.removeAllViews()

    scores1.forEach { score ->
        val scoreTextView = TextView(context).apply {
            gravity = Gravity.CENTER
            text = score.label
            setTextColor(ContextCompat.getColor(context, R.color.black))
            setPadding(
                15,
                15,
                15,
                15
            )

            if (score.iconId != 0) {
                compoundDrawablePadding = 10
                setCompoundDrawablesWithIntrinsicBounds(0, 0, score.iconId, 0)
            }
        }

        scoreTextView.setOnClickListener {
            onClickCallback(score.label.orEmpty())
        }

        circularLayout.addView(scoreTextView)
    }
}